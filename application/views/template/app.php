<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <meta http-equiv="refresh" content="1800;url=<?php echo site_url('index/logout') ?>" />
  <title><?php echo $this->config->item('webname') ?></title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo base_url('public/modules/css/bootstrap.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('public/modules/css/all.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('public/modules/select2/select2.min.css') ?>">
  <!-- daterange -->
  <link rel="stylesheet" href="<?php echo base_url('public/modules/bootstrap-daterangepicker/daterangepicker.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('public/modules/datepicker/css/datepicker.css') ?>">
  <!-- CSS Libraries -->
  <link rel="stylesheet" href="<?php echo base_url() ?>public/css/style.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>public/css/components.css">
  <!-- datatables -->
  <link rel="stylesheet" href="<?php echo base_url('public/modules/datatables/bootstrap/datatables.bootstrap.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('public/modules/datatables/datatables-bs4/css/dataTables.bootstrap4.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('public/modules/datatables/select-datatable.css') ?>">

  <link rel="stylesheet" href="<?php echo base_url('public/modules/sweetalert2-theme-bootstrap-4/bootstrap-4.css') ?>">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo base_url('public/modules/chosen/bootstrap-chosen.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('public/modules/chosen/chosen.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('public/modules/summernote/summernote-lite.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('public/modules/summernote/summernote-bs4.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/loading_page.css'); ?>">
  <link href="<?php echo base_url('public/modules/bootstrap3-editable') ?>/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" />
  <link href="<?php echo base_url('public/modules/iziToast/dist/css/') ?>iziToast.min.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/modules/fontawesome/css/all.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/modules/select2/select2.min.css') ?>">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/modules/daterange/bootstrap-timepicker.min.css') ?>">

  <style type="text/css">
    .brand {
      color: #FFAE01;
    }

    .bg-warning {
      color: #FEB415;
      background: #FEB415;
      background-color: #FEB415;
    }
    #ui-datepicker-div{
  z-index: 1051!important;
}
  </style>

</head>
<?php
if (isset($output->css_files)) {
    foreach ($output->css_files as $file) {
        echo '<link type="text/css" rel="stylesheet" href="' . $file . '"/>';
    }
}
?>

<body>
  <div id="spinner-front">
    <img src="<?php echo base_url('public/ajax-loader.gif'); ?>" /><br>
    Proses Sedang Berjalan, Harap Ditunggu...
  </div>
  <div id="spinner-back"></div>
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg bg-warning"></div>
      <nav class="navbar navbar-expand-lg main-navbar">
        <div class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
          </ul>
        </div>
        <ul class="navbar-nav navbar-right">
          <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
              <img alt="image" src="<?php echo base_url() ?>public/img/logo.png" class="mr-1">
              <div class="d-sm-none d-lg-inline-block">Hi, <?php echo $this->session->userdata('t_username'); ?></div>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              <a href="<?php echo site_url('/index/logout'); ?>" class="dropdown-item has-icon text-danger">
                <i class="fas fa-sign-out-alt"></i> Logout
              </a>
            </div>
          </li>
        </ul>
      </nav>
      <div class="main-sidebar">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a class="brand" href="<?php echo site_url('admin/Index'); ?>">Administrator</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="<?php echo site_url('index'); ?>">R</a>
          </div>
          <ul class="sidebar-menu">
            <li class="menu-header"><?php echo $this->config->item('webname') ?></li>
            <li class="<?php echo is_active_page('index', 'active') ?>">
              <a class="nav-link" href="<?php echo site_url('index'); ?>"><i class="fa fa-home"></i>
                <span>Beranda</span></a>
            </li>
              <?php if (in_array("akun.access", $userMenus)) : ?>
                <li class="<?php echo is_active_page('akun', 'active') ?>">
                  <a class="nav-link" href="<?php echo site_url('akun/index') ?>"><i class="fas fa-th-list"></i><span>Akun</span></a>
                </li>
              <?php endif; ?>
              <?php if (in_array("lembaga.access", $userMenus)) : ?>
                <li class="<?php echo is_active_page('lembaga', 'active') ?>">
                  <a class="nav-link" href="<?php echo site_url('lembaga/index') ?>"><i class="fas fa-image"></i><span>Lembaga</span></a>
                </li>
              <?php endif; ?>
              <?php if (in_array("devisi.access", $userMenus)) : ?>
                <li class="<?php echo is_active_page('devisi', 'active') ?>">
                  <a class="nav-link" href="<?php echo site_url('devisi/index') ?>"><i class="fas fa-tags"></i><span>Devisi</span></a>
                </li>
              <?php endif; ?>
              <li class="nav-item dropdown <?php echo is_active_page('kategori_artikel', 'active');
                                            echo is_active_page('artikel', 'active');
                                            echo is_active_page('slider', 'active'); ?>">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-newspaper"></i><span>Artikel</span></a>
                <ul class="dropdown-menu">
                  <li class="<?php echo is_active_page('kategori_artikel', 'active') ?>">
                    <a class="nav-link" href="<?php echo site_url('kategori_artikel/index'); ?>"><span>Kategori Artikel</span></a>
                  </li>
                  <?php if (in_array("artikel.access", $userMenus)) : ?>
                    <li class="<?php echo is_active_page('artikel', 'active') ?>">
                      <a class="nav-link" href="<?php echo site_url('artikel/index'); ?>"><span>Blog Artikel</span></a>
                    </li>
                  <?php endif; ?>
                  <?php if (in_array("slider.access", $userMenus)) : ?>
                    <li class="<?php echo is_active_page('slider', 'active') ?>">
                      <a class="nav-link" href="<?php echo site_url('slider/index'); ?>"><span>Slider Highlight</span></a>
                    </li>
                  <?php endif; ?>
                </ul>
              </li>
              <li class="nav-item dropdown <?php echo is_active_page('agen', 'active');
                                            echo is_active_page('kantor_cabang', 'active');
                                            echo is_active_page('pegawai', 'active'); ?>">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-th-list"></i><span>Data Pokok</span></a>
                <ul class="dropdown-menu">
                  <?php if (in_array("agen.access", $userMenus)) : ?>
                    <li class="<?php echo is_active_page('agen', 'active') ?>">
                      <a class="nav-link" href="<?php echo site_url('agen/index') ?>"><span>Agen</span></a>
                    </li>
                  <?php endif; ?>
                  <?php if (in_array("pegawai.access", $userMenus)) : ?>
                    <li class="<?php echo is_active_page('pegawai', 'active') ?>">
                      <a class="nav-link" href="<?php echo site_url('pegawai/index') ?>"><span>Pegawai</span></a>
                    </li>
                  <?php endif; ?>
                  <?php if (in_array("kantor_cabang.access", $userMenus)) : ?>
                    <li class="<?php echo is_active_page('kantor_cabang', 'active') ?>">
                      <a class="nav-link" href="<?php echo site_url('kantor_cabang/index') ?>"><span>Cabang</span></a>
                    </li>
                  <?php endif; ?>
                </ul>
              </li>
              <li class="nav-item dropdown <?php echo is_active_page('jamaah', 'active');
                                            echo is_active_page('jamaah_birthday', 'active') ?>">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-users"></i><span>Jama'ah</span></a>
                <ul class="dropdown-menu">
                  <?php if (in_array("jamaah.access", $userMenus)) : ?>
                    <li class="<?php echo is_active_page('jamaah', 'active') ?>">
                      <a class="nav-link" href="<?php echo site_url('jamaah/index') ?>"><span>Data Jamaah</span></a>
                    </li>
                  <?php endif; ?>
                  <?php if (in_array("jamaah_birthday.access", $userMenus)) : ?>
                    <li class="<?php echo is_active_page('jamaah_birthday', 'active') ?>">
                      <a class="nav-link" href="<?php echo site_url('jamaah_birthday/index') ?>"><span>Ulang tahun Jamaah</span></a>
                    </li>
                  <?php endif; ?>
                </ul>
              </li>
              <li class="nav-item dropdown d-none <?php echo is_active_page('tabrur', 'active');
                                            echo is_active_page('tabah', 'active') ?>">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-gem"></i><span>Tabungan</span></a>
                <ul class="dropdown-menu">
                  <li class="<?php echo is_active_page('tabrur', 'active') ?>">
                    <a class="nav-link" href="<?php echo site_url('tabrur/index') ?>"><span>Tabrur</span></a>
                  </li>
                  <?php if (in_array("tabah.access", $userMenus)) : ?>
                    <li class="<?php echo is_active_page('tabah', 'active') ?>">
                      <a class="nav-link" href="<?php echo site_url('tabah/index') ?>">Tabah</span></a>
                    </li>
                  <?php endif; ?>
                </ul>
              </li>



              <li class="nav-item dropdown <?php echo is_active_page('rekap', 'active') ?>">
                <a href="#" class="nav-link has-dropdown"><i class="fa fa-calendar"></i><span>Rekap</span></a>
                <ul class="dropdown-menu">
                  <li class="<?php echo (strpos(uri_string(), 'kota') !== false ? 'active' : '') ?>">
                    <a class="nav-link" href="<?php echo site_url('rekap/index/kota') ?>"><span>Berdasarkan Kota</span></a>
                  </li>
                  <?php if (in_array("rekap.access", $userMenus)) : ?>
                    <li class="<?php echo (strpos(uri_string(), 'marketing') !== false ? 'active' : '') ?>">
                      <a class="nav-link" href="<?php echo site_url('rekap/index/marketing') ?>"><span>Berdasarkan Marketing</span></a>
                    </li>
                  <?php endif; ?>
                  <?php if (in_array("rekap.access", $userMenus)) : ?>
                    <li class="<?php echo (strpos(uri_string(), 'seat') !== false ? 'active' : '') ?>">
                      <a class="nav-link" href="<?php echo site_url('rekap/index/seat') ?>"><span>Berdasarkan Seat</span></a>
                    </li>
                  <?php endif; ?>
                </ul>
              </li>

            <li><a class="nav-link" href="<?php echo site_url('index/logout'); ?>"><i class="fas fa-sign-out-alt"></i>
                <span>Log Out</span></a>
            </li>

          </ul>
        </aside>
      </div>
      <!-- Main Content -->
      {CONTENT}

      <footer class="main-footer">
        <div class="footer-left">
          <!-- Copyright &copy; 2018 <div class="bullet"></div> Design By <a href="https://nauval.in/">Muhamad Nauval Azhar</a> -->
        </div>
        <div class="footer-right">
          v2.3.0
        </div>
      </footer>
    </div>
  </div>

</body>

</html>