<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/Format.php';

use Restserver\Libraries\REST_Controller;


class Customer extends REST_Controller{

    private $ok = '200';
    private $bad = '400';
    private $unauthorized = '401';
    private $notfound = '404';
    private $error = '500';

    function __construct($config = 'rest') {

        parent::__construct($config);
        $this->methods['data_post']['limit'] = 100; // 100 requests per hour per data/key
        $this->load->model('api/api_customer', 'customer');
    }

    public function registrasi_post()
    {
        $data = json_decode(trim(file_get_contents('php://input')), true);

        $result = $this->customer->registrasi_customer($data);
        if (is_array($result) && $result!=NULL) {
            if ($result['status']=='ok') {
                $this->response(['status'=>$this->ok,'message'=>$result['message'],'data'=>$result['data']], REST_Controller::HTTP_OK);
            }else {
                $this->response(['status'=>$this->notfound,'message'=>$result['message'],'data'=>$result['data']], REST_Controller::HTTP_NOT_FOUND);
            }
        }else {
            $this->response(['status'=>$this->notfound,'message'=>$result['message'],'data'=>$result['data']], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function verifikasi_post()
    {
        $data = json_decode(trim(file_get_contents("php://input")), true);
        if (!is_array($data)) {
            $this->response(
                ['status'=>$this->bad,
                'message'=>'Data yang dikirim tidak boleh kosong',
                'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
        }else {
            $result = $this->customer->verifikasi($data);

            if ($result['status']!='failed' && $result['data']!=NULL) {

                $getToken = $this->customer->check_user($result['data']['username']);

                $token = $this->_create_token($getToken['data'], $result['data']['username']);
                $result['data']['token'] = $token;

                $this->response([
                    'status'=>$this->ok,
                    'message'=>$result['message'],
                    'data'=>$result['data']],REST_Controller::HTTP_OK);
            }else {
                $this->response([
                    'status'=>$this->error,
                    'message'=>$result['message'],
                    'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }
        }
    }

    public function resetHP_post()
    {
        $data = json_decode(trim(file_get_contents("php://input")), true);
        if (!empty($data)) {
            $result = $this->customer->resetHP($data);

            if (!empty($result)) {
                $this->response([
                    'status'=>$this->ok,
                    'message'=>$result['message'],
                    'data'=>$result['data']], REST_Controller::HTTP_OK);
            }else {
                $this->response([
                    'status'=>$this->bad,
                    'message'=>$result['message'],
                    'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
            }
        }else {
            $this->response([
                'status'=>$this->error,
                'message'=>'Email verifikasi tidak boleh dikosongkan',
                'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function gantiNomorHP_post()
    {
        $data = json_decode(trim(file_get_contents("php://input")), true);
        if (!empty($data)) {

            $result = $this->customer->ubah_nomor($data);

            if (!empty($result) &&$result!=NULL) {

                $this->response([
                    'status'=>$this->ok,
                    'message'=>$result['message'],
                    'data'=>$result['data']], REST_Controller::HTTP_OK);
            }else {

                $this->response([
                    'status'=>$this->bad,
                    'message'=>$result['message'],
                    'data'=>''],REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }

        }else {
            $this->response([
                'status'=>$this->error,
                'message'=>'Data perubahan nomor HP tidak boleh kosong',
                'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function verifikasiNomorHP_post()
    {
        $data = json_decode(trim(file_get_contents("php://input")), true);
        if (!empty($data)) {
            $result = $this->customer->verifikasiPerubahanHP($data);

            if (!empty($result) && $result!=NULL) {

                $this->response([
                    'status'=>$this->ok,
                    'message'=>$result['message'],
                    'data'=>$result['data']], REST_Controller::HTTP_OK);

            }else {

                $this->response([
                    'status'=>$this->error,
                    'message'=>$result['message'],
                    'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }

        }else {

            $this->response([
                'status'=>$this->bad,
                'message'=>'Data parameter yang dikirimkan tidak boleh kosong',
                'data'=>''],REST_Controller::HTTP_BAD_REQUEST);
        }
    }


    public function lupaSandi_post()
    {
        $data = json_decode(trim(file_get_contents("php://input")), true);

        if (!empty($data)) {

            $result = $this->customer->lupaSandi($data);

            if (!empty($result) && $result!=NULL) {

                $this->response([
                    'status'=>$this->ok,
                    'message'=>$result['message'],
                    'data'=>$result['data']], REST_Controller::HTTP_OK);
            }else {
                $this->response([
                    'status'=>$this->error,
                    'mesage'=>$result['message'],
                    'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }
        }else {
            $this->response([
                'status'=>$this->bad,
                'message'=>'Data parameter yang dikirimkan tidak boleh kosong',
                'data'=>''],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function updateSandi_post()
    {
        $data = json_decode(trim(file_get_contents("php://input")), true);

        if (!empty($data)) {

            $result = $this->customer->updateSandi($data);

            if (!empty($result) && $result!=NULL) {

                $this->response([
                    'status'=>$this->ok,
                    'message'=>$result['message'],
                    'data'=>$result['data']], REST_Controller::HTTP_OK);

            }else {
                $this->response([
                    'status'=>$this->error,
                    'mesage'=>$result['message'],
                    'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }

        }else {
            $this->response([
                'status'=>$this->bad,
                'message'=>'Data parameter yang dikirimkan tidak boleh kosong',
                'data'=>''],REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function referal_post()
    {
        $headers = $this->input->request_headers();

      $headers = array_change_key_case($headers,CASE_LOWER);

      if (array_key_exists('authorization', $headers) && !empty($headers['authorization'])) {

        $token = $headers['authorization'];

            $decoded_token = AUTHORIZATION::validateToken($token);

            if ($decoded_token!=FALSE && property_exists($decoded_token, "id_user")) {

                $data = json_decode(trim(file_get_contents("php://input")), true);

                if ($data['id_user']!=NULL) {

                $result = $this->customer->getReferal($data['id_user']);

                if (!empty($result) && $result['status']!='failed') {

                    $this->response([
                        'status'=>$this->ok,
                        'message'=>$result['message'],
                        'data'=>$result['data']], REST_Controller::HTTP_OK);
                }else {

                    $this->response([
                        'status'=>$this->bad,
                        'message'=>$result['message'],
                        'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
                }
            }else {

                $this->response([
                    'status'=>$this->bad,
                    'message'=>'Parameter tidak boleh kosong',
                    'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
            }

            }else {

                $this->response([
                    'status' => $this->unauthorized,
                    'message' => 'Unathorized/Invalid Token',
                    'data'=>''], REST_Controller::HTTP_UNAUTHORIZED);
            }

        }else {
            $this->response([
                'status' => $this->bad,
                'message' => 'Token tidak ditemukan.',
                'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
        }

    }

    public function edit_post()
    {
        $headers = $this->input->request_headers();

      $headers = array_change_key_case($headers,CASE_LOWER);

      if (array_key_exists('authorization', $headers) && !empty($headers['authorization'])) {

        $token = $headers['authorization'];
            $decoded_token = AUTHORIZATION::validateToken($token);

            if ($decoded_token!= false && property_exists($decoded_token, "id_user")) {

                $data = json_decode(trim(file_get_contents("php://input")), true);

                if (!empty($data['id_user'])) {

                    $result = $this->customer->editCustomer($data['id_user']);

                    if (!empty($result) && $result['status']!='failed') {

                        $this->response([
                            'status'=>$this->ok,
                            'message'=>$result['message'],
                            'data'=>$result['data']],REST_Controller::HTTP_OK);
                    }else {

                        $this->response([
                            'status'=>$this->error,
                            'message'=>$result['message'],
                            'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
                    }
                }else {

                    $this->response([
                        'status'=>$this->bad,
                        'message'=>'Data Parameter tidak boleh kosong',
                        'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
                }

            }else {

                $this->response([
                    'status' => $this->unauthorized,
                    'message' => 'Unathorized/Invalid Token',
                    'data'=>''], REST_Controller::HTTP_UNAUTHORIZED);
            }

        }else {
            $this->response([
                'status' => $this->bad,
                'message' => 'Token tidak ditemukan.',
                'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
        }

    }


    public function update_post()
    {

        $headers = $this->input->request_headers();

      $headers = array_change_key_case($headers,CASE_LOWER);

      if (array_key_exists('authorization', $headers) && !empty($headers['authorization'])) {

        $token = $headers['authorization'];
            $decoded_token = AUTHORIZATION::validateToken($token);

            if ($decoded_token != false && property_exists($decoded_token, "id_user")) {

                $data['id_cabang'] = $this->input->post('id_cabang');
                $data['nik'] = $this->input->post('nik');
                $data['nama'] = $this->input->post('nama');
                $data['nama_ayah'] = $this->input->post('nama_ayah');
                $data['tempat_lahir'] = $this->input->post('tempat_lahir');
                $data['tanggal_lahir'] = $this->input->post('tanggal_lahir');
                $data['jenis_kelamin'] = $this->input->post('jenis_kelamin');
                $data['status'] = $this->input->post('status');
                $data['nomor_hp'] = $this->input->post('nomor_hp');
                $data['email'] = $this->input->post('email');
                $data['pendidikan'] = $this->input->post('pendidikan');
                $data['pekerjaan'] = $this->input->post('pekerjaan');
                $data['penghasilan'] = $this->input->post('penghasilan');
                $data['id_provinsi'] = $this->input->post('id_provinsi');
                $data['id_kabupaten'] = $this->input->post('id_kabupaten');
                $data['id_kecamatan'] = $this->input->post('id_kecamatan');
                $data['alamat'] = $this->input->post('alamat');

                $data_lama = $this->customer->editCustomer($this->input->post('fk_id_user'));

                if (!empty($this->input->post('ubah_foto')) && $this->input->post('ubah_foto')=='1') {

                    if ($data_lama['data']['customer']['url_foto']!='' || $data_lama['data']['customer']['url_foto']!=NULL) {
                        if (file_exists('./'.$data_lama['data']['customer']['url_foto'])) {
                            unlink('./'.$data_lama['data']['customer']['url_foto']);
                        }
                    }

                    if (isset($_FILES['url_foto']['name'])) {
                        list($width, $height) = getimagesize($_FILES['url_foto']['tmp_name']);
                        $config['upload_path'] = 'files/profil/'; //path folder file upload
                        $config['allowed_types'] = 'gif|jpg|jpeg|png|jpeg|bmp'; //type file yang boleh di upload
                        $config['max_size'] = '2000';
                        $config['file_name'] = "profile_" . date('ymdhis'); //enkripsi file name upload
                        $this->load->library('upload');
                        $this->upload->initialize($config);
                        if ($this->upload->do_upload('url_foto')) {
                            $file_foto = $this->upload->data();
                            $config['image_library'] = 'gd2';
                            $config['source_image'] = './files/profil/' . $file_foto['file_name'];
                            $config['create_thumb'] = FALSE;
                            $config['maintain_ratio'] = TRUE;
                            $config['quality'] = '50%';
                            $config['width'] = round($width / 2);
                            $config['height'] = round($height / 2);
                            $config['new_image'] = './files/profil/' . $file_foto['file_name'];
                            $this->load->library('image_lib');
                            $this->image_lib->initialize($config);
                            $this->image_lib->resize();
                            $nama_foto = 'files/profil/' . $file_foto['file_name'];
                            $data['url_foto'] = $nama_foto;
                        }
                    }
                }else {
                    $data['url_foto'] = $data_lama['data']['customer']['url_foto'];
                }

                $result = $this->customer->updateCustomer($data, $this->input->post('fk_id_user'));

                if ($result['status'] != 'failed') {

                    $this->response([
                        'status'=>$this->ok,
                        'message'=>$result['message'],
                        'data'=>$result['data']], REST_Controller::HTTP_OK);
                }else {

                    $this->response([
                        'status'=>$this->error,
                        'message'=>$result['message'],
                        'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
                }

            }else {

                $this->response([
                    'status' => $this->unauthorized,
                    'message' => 'Unathorized/Invalid Token',
                    'data'=>''], REST_Controller::HTTP_UNAUTHORIZED);
            }

        }else {
            $this->response([
                'status' => $this->bad,
                'message' => 'Token tidak ditemukan.',
                'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
        }

    }

    private function _create_token($data, $username) {
        $token = array();
        $token['id_user'] = $data['id_user'];
        // $token['id_group'] = $data['id_group'];

        $refresh_token = array();
        $refresh_token['username'] = $username;

        try {
            $jwt = AUTHORIZATION::generateToken($token);
            $refresh = AUTHORIZATION::generateToken($refresh_token);
        } catch (Exception $error) {
            $jwt = null;
            $refresh = null;
        }

        $CI =& get_instance();
        $result = array();
        $result['token'] = $jwt;
        $result['expires'] = ($CI->config->item('token_timeout') > 0) ? ($CI->config->item('token_timeout') * 60) : 'never';
        $result['refresh_token'] = $refresh;

        return $result;
    }

    public function index_get() {
        $this->response([
            'status' => $this->bad,
            'error' => 'Bad Request'
                ], REST_Controller::HTTP_BAD_REQUEST);
    }




}
