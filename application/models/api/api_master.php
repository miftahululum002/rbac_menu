<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api_master extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function hotel_data() {
        $get = $this->db->query("
            SELECT id_hotel, nama_hotel, bintang
            FROM m_hotel
            WHERE is_active = '1'
            ORDER BY nama_hotel ASC
        ");
        if ($get->num_rows() == 0) {
            return ["status" => "failed", "message" => "Data tidak ditemukan."];
        }

        $i = 0;
        foreach ($get->result() as $key => $r) {
            $result[$i]['id_hotel'] = $r->id_hotel;
            $result[$i]['nama_hotel'] = $r->nama_hotel;
            $result[$i]['bintang'] = $r->bintang;
            $i++;
        }

        // serve
        return ["status" => "ok", "data" => $result];
    }

    function bandara_data() {
        $get = $this->db->query("
            SELECT
            id_bandara, iata, nama_bandara,kota
            FROM m_bandara
            ORDER BY nama_bandara ASC
        ");
        if ($get->num_rows() == 0) {
            return ["status" => "failed", "message" => "Data tidak ditemukan."];
        }

        $i = 0;
        foreach ($get->result() as $key => $r) {
            $result[$i]['id_bandara'] = $r->id_bandara;
            $result[$i]['iata'] = $r->iata;
            $result[$i]['nama_bandara'] = $r->nama_bandara;
            $result[$i]['kota'] = $r->kota;
            $i++;
        }

        // serve
        return ["status" => "ok", "data" => $result];
    }

    public function biaya_tambahan()
    {
        $query = $this->db->query("SELECT id_biaya, nama_biaya, biaya_usd, biaya_rupiah from m_biaya_tambahan where status=1");
        if ($query->num_rows()!=0) {
            return ['status'=>'ok','message'=>'data get','data'=>$query->result()];
        }else {
            return ['status'=>'failied','message'=>'data not found','data'=>''];
        }
    }

    public function getWilayah()
    {
        $result = null;
        $prov = array();
        $kab = array();
        $kec = array();
        $i = 0;
        $j = 0;
        $k = 0;
        $provinsi = $this->db->query("SELECT * FROM ref_provinsi ORDER by nama_propinsi");
        if ($provinsi->num_rows()>0) {
            foreach ($provinsi->result() as $rows) {
                $prov[$i]['kode_propinsi'] = $rows->kode_propinsi;
                $prov[$i]['nama_propinsi'] = $rows->nama_propinsi;
                $i++;
            }

            $kabupaten = $this->db->query("SELECT * from ref_kab_kota order by nama_kab_kota");
            foreach ($kabupaten->result() as $rows) {
                $kab[$j]['kode_kab_kota'] = $rows->kode_kab_kota;
                $kab[$j]['nama_kab_kota'] = $rows->nama_kab_kota;
                $j++;
            }

            $kecamatan = $this->db->query("SELECT * from ref_kecamatan order by nama_kecamatan");
            foreach ($kecamatan->result() as $rows) {
                $kec[$k]['kode_kecamatan'] = $rows->kode_kecamatan;
                $kec[$k]['nama_kecamatan'] = $rows->nama_kecamatan;
                $k++;
            }

            $result['provinsi'] = $prov;
            $result['kota_kabupaten'] = $kab;
            $result['kecamatan'] = $kec;

            return [
                'status'=>'success',
                'message'=>'Data referensi wilayah tersedia',
                'data'=>$result];


        }else {
            return [
                'status'=>'failed',
                'message'=>'Data is empty',
                'data'=>''];
        }
    }

    public function getKabupatenByProvinsi($data)
    {
        if (!empty($data)) {

            $this->db->select("*");
            $this->db->from("ref_kab_kota");
            $this->db->where("kode_provinsi", $data['kode_provinsi']);
            $result = $this->db->get();
            $i=0;
            $kabupaten = array();
            foreach ($result->result() as $rows) {
                $kabupaten[$i]['kode_kab_kota'] = $rows->kode_kab_kota;
                $kabupaten[$i]['nama_kab_kota'] = $rows->nama_kab_kota;
                $i++;
            }
            return [
                'status'=>'success',
                'message'=>'Data kota/kabupaten ditemukan',
                'data'=>$kabupaten];
        }else {
            return [
            'status'=>'failed',
            'message'=>'Parameter tidak boleh kosong',
            'data'=>''];
        }
    }


    public function getKecamatanByKabupaten($data)
    {
        if (!empty($data)) {

            $this->db->select("*");
            $this->db->from("ref_kecamatan");
            $this->db->where("kode_kab_kota", $data['kode_kota']);
            $result = $this->db->get();
            $i=0;
            $kecamatan = array();
            foreach ($result->result() as $rows) {
                $kecamatan[$i]['kode_kecamatan'] = $rows->kode_kecamatan;
                $kecamatan[$i]['nama_kecamatan'] = $rows->nama_kecamatan;
                $i++;
            }
            return [
                'status'=>'success',
                'message'=>'Data kecamatan ditemukan',
                'data'=>$kecamatan];
        }else {
            return [
            'status'=>'failed',
            'message'=>'Parameter tidak boleh kosong',
            'data'=>''];
        }
    }

    public function getKantorCabang()
    {
        $result = array();
        $get = $this->db->query("SELECT id_cabang, nama_cabang from cabang ORDER BY nama_cabang ASC");

        if ($get->num_rows()>0) {

            $i=0;
            foreach ($get->result() as $rows) {
                $result[$i]['id_cabang'] = $rows->id_cabang;
                $result[$i]['nama_cabang'] = $rows->nama_cabang;
                $i++;
            }

            return [
                'status'=>'success',
                'message'=>'Data referensi kantor cabang ditemukan',
                'data'=>$result];
        }else {

            return [
                'status'=>'failed',
                'message'=>'Data referensi kantor cabang kosong',
                'data'=>''];
        }
    }


    public function kirimSaran($data)
    {
        if (!empty($data)) {

            $this->db->insert('kotak_masuk', $data);

            if ($this->db->affected_rows()) {
                return [
                    'status'=>'success',
                    'message'=>'Terimakasih atas saran yang anda berikan. Semua saran anda akan menjadi masukan yang bermanfaat bagi kami',
                    'data'=>'1'];
            }else {
                return [
                    'status'=>'failed',
                    'message'=>'Terjadi kesalahan pada proses pengiriman saran',
                    'data'=>'0'];
            }
        }else {
            return [
                'status'=>'failed',
                'message'=>'Data saran tidak boleh kosong',
                'data'=>''];
        }
    }

}
