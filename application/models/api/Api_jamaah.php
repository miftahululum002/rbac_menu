<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api_jamaah extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function getJamaahByPemesanan($id)
    {
        $query = $this->db->query("SELECT * from um_pemesanan_jamaah where REPLACE(fk_id_pemesanan,'-','')=?", array(str_replace("-", "", $id)));
        if ($query->num_rows()!=0) {
            $i=0;
            foreach ($query->result() as $rows) {
                $result[$i]['id_jamaah'] = $rows->id_jamaah;
                $result[$i]['nama_lengkap'] = $rows->nama_lengkap;
                $result[$i]['jenis_identitas'] = $rows->jenis_identitas;
                $result[$i]['nomor_identitas'] = $rows->nomor_identitas;
                $result[$i]['jenis_kelamin'] = $rows->jenis_kelamin;
                $result[$i]['kategori'] = $rows->kategori;
                $result[$i]['tempat_lahir'] = $rows->tempat_lahir;
                $result[$i]['tanggal_lahir'] = $rows->tanggal_lahir;
                $result[$i]['status_kawin'] = $rows->status_kawin;
                $result[$i]['pendidikan'] = $rows->pendidikan;
                $result[$i]['pekerjaan'] = $rows->pekerjaan;
                $result[$i]['alamat_ktp'] = $rows->alamat_ktp;
                $result[$i]['email'] = $rows->email;
                $result[$i]['nomor_hp'] = $rows->nomor_hp;
                $result[$i]['pilihan_kamar'] = $rows->pilihan_kamar;
                $result[$i]['harga'] = $rows->harga;
                $i++;
            }

            return ['status'=>'ok','message'=>'data jamaah ditemukan','data'=>$result];
        }else {
            return ['status'=>'failed','message'=>'data jamaah tidak ditemukan','data'=>0];

        }
    }

    public function edit($id)
    {
        $query = $this->db->query("SELECT * from um_pemesanan_jamaah where REPLACE(id_jamaah,'-','')=?", array(str_replace("-", "", $id)));
        if ($query->num_rows()!=0) {
            $k = $query->row();
            $result['fk_id_pemesanan'] = $k->fk_id_pemesanan;
            $result['id_jamaah'] = $k->id_jamaah;
            $result['nama_lengkap'] = $k->nama_lengkap;
            $result['jenis_identitas'] = $k->jenis_identitas;
            $result['nomor_identitas'] = $k->nomor_identitas;
            $result['jenis_kelamin'] = $k->jenis_kelamin;
            $result['kategori'] = $k->kategori;
            $result['tempat_lahir'] = $k->tempat_lahir;
            $result['tanggal_lahir'] = $k->tanggal_lahir;
            $result['status_kawin'] = $k->status_kawin;
            $result['pendidikan'] = $k->pendidikan;
            $result['pekerjaan'] = $k->pekerjaan;
            $result['id_provinsi'] = $k->id_provinsi;
            $result['id_kota'] = $k->id_kota;
            $result['id_kecamatan'] = $k->id_kecamatan;
            $result['kode_pos'] = $k->kode_pos;
            $result['alamat_ktp'] = $k->alamat_ktp;
            $result['alamat_domisili'] = $k->alamat_domisili;
            $result['nama_ayah'] = $k->nama_ayah;
            $result['nama_ibu'] = $k->nama_ibu;
            $result['email'] = $k->email;
            $result['nomor_hp'] = $k->nomor_hp;
            $result['nomor_hp_cadangan'] = $k->nomor_hp_cadangan;
            $result['keterangan'] = $k->keterangan;
            $result['nama_ahli_waris'] = $k->nama_ahli_waris;
            $result['nomor_hp_ahli_waris'] = $k->nomor_hp_ahli_waris;
            $result['alamat_ahli_waris'] = $k->alamat_ahli_waris;
            $result['nomor_paspor'] = $k->nomor_paspor;
            $result['nama_paspor'] = $k->nama_paspor;
            $result['foto_profil'] = $k->foto_profil;
            $result['scan1_paspor'] = $k->scan1_paspor;
            $result['scan2_paspor'] = $k->scan2_paspor;
            $result['tanggal_dikeluarkan'] = $k->tanggal_dikeluarkan;
            $result['tanggal_berlaku'] = $k->tanggal_berlaku;
            $result['kantor_penerbit'] = $k->kantor_penerbit;
            $result['ukuran_baju'] = $k->ukuran_baju;
            $result['pelayanan_khusus'] = $k->pelayanan_khusus;
            $result['is_smoking'] = $k->is_smoking;
            $result['keterangan_kamar'] = $k->keterangan_kamar;
            $result['kewarganegaraan'] = $k->kewarganegaraan;
            $result['pilihan_kamar'] = $k->pilihan_kamar;
            $result['harga'] = $k->harga;
            $result['id_referensi_pegawai'] = $k->id_referensi_pegawai;
            $result['url_invoice'] = $k->url_invoice;

            $biaya_tambahan = array();

            $get = $this->db->query("SELECT * from um_pemesanan_biaya_tambahan where REPLACE(id_jamaah,'-','')=?", array(str_replace("-", "", $id)));

            if ($get->num_rows()>0) {
                $i=0;
                foreach ($get->result() as $rows) {
                    $biaya_tambahan[$i]['id_biaya']=$rows->id_biaya;
                    $biaya_tambahan[$i]['id_jamaah']=$rows->id_jamaah;
                    $biaya_tambahan[$i]['fk_biaya_tambahan']=$rows->fk_biaya_tambahan;
                    $biaya_tambahan[$i]['mata_uang']=$rows->mata_uang;
                    $biaya_tambahan[$i]['jumlah']=$rows->jumlah;
                    $i++;
                }
            }else {
                $biaya_tambahan = array();
            }
            $result['biaya_tambahan_jamaah'] = $biaya_tambahan;

            return ['status'=>'ok','message'=>'data jamaah ditemukan','data'=>$result];
        }else {
            return ['status'=>'failed','message'=>'data jamaah tidak ditemukan','data'=>'0'];

        }
    }


    public function update($data, $id)
    {
        $this->db->where("REPLACE(id_jamaah,'-','')", str_replace("-", '', $id));
        $this->db->update('um_pemesanan_jamaah', $data);
        if ($this->db->affected_rows()) {
            return ['status'=>'ok','message'=>'Data jamaah berhasil diupdate','data'=>'1'];
        }else {
            return ['status'=>'failed','message'=>'Data jamaah gagal diupdate','data'=>'0'];
        }
    }

    public function get_detail($id_jamaah)
    {
        $result = array();
        $this->db->select("up.prefix, kp.nama_program, gk.nama_grup, gk.tanggal_keberangkatan, upm.id_jamaah, upm.nama_lengkap, upm.jenis_identitas, upm.nomor_identitas, upm.jenis_kelamin,
            upm.kategori, upm.nomor_hp,upm.pilihan_kamar, upm.harga");
        $this->db->from("um_pemesanan_jamaah upm");
        $this->db->where("REPLACE(upm.id_jamaah,'-','')", $id_jamaah);
        $this->db->join("um_pemesanan up","REPLACE(up.id_pemesanan,'-','')=REPLACE(upm.fk_id_pemesanan,'-','')",'left');
        $this->db->join("um_kelas_program kp","REPLACE(kp.id_kelas_program,'-','')=REPLACE(up.fk_program_umrah,'-','')",'left');
        $this->db->join("um_grup_keberangkatan gk","REPLACE(gk.id_grup,'-','')=REPLACE(kp.fk_grup_keberangkatan,'-','')",'left');
        $get = $this->db->get();

        if ($get->num_rows()>0) {
            $r = $get->row();

            $result['prefix'] = $r->prefix;
            $result['nama_program'] = $r->nama_program;
            $result['nama_group'] = $r->nama_grup;
            $result['tanggal_keberangkatan'] = $r->tanggal_keberangkatan;
            $result['id_jamaah'] = $r->id_jamaah;
            $result['nama_lengkap'] = $r->nama_lengkap;
            $result['jenis_identitas'] = $r->jenis_identitas;
            $result['nomor_identitas'] = $r->nomor_identitas;
            $result['jenis_kelamin'] = $r->jenis_kelamin;
            $result['kategori'] = $r->kategori;
            $result['pilihan_kamar'] = $r->pilihan_kamar;
            $result['nomor_hp'] = $r->nomor_hp;
            $result['harga'] = $r->harga;

            $this->db->select("nama_biaya, mata_uang, jumlah");
            $this->db->from("um_pemesanan_biaya_tambahan bt");
            $this->db->where("REPLACE(bt.id_jamaah,'-','')", $id_jamaah);
            $this->db->join("m_biaya_tambahan mbt","mbt.id_biaya=bt.fk_biaya_tambahan","left");
            $bt = $this->db->get();

            $biaya = array();
            $no=0;
            foreach ($bt->result() as $rows) {
                $biaya[$no]['nama_biaya'] = $rows->nama_biaya;
                $biaya[$no]['mata_uang'] = $rows->mata_uang;
                $biaya[$no]['jumlah'] = $rows->jumlah;
                $no++;
            }
            $result['biaya_tambahan'] = $biaya;
            return [
                'status'=>'ok',
                'data'=>$result,
                'message'=>'data ditemukan'];
        }else {
            return ['status'=>'failed','message'=>'Data jamaah gagal diupdate','data'=>'0'];
        }

    }

}
