<?php

// PWMP API

class APIHELPER
{

    public static function role_code($data) {
        $result = null;
        switch(true) {
            case ($data['id'] === 1 || strpos(strtolower($data['nama_role']), 'Holding App') !== false):
                $result = 'ADM1';
                break;
            case ($data['id'] === 2 || strpos(strtolower($data['nama_role']), 'Adminstrator') !== false):
                $result = 'ADM2';
                break;
            case ($data['id'] === 3 || strpos(strtolower($data['nama_role']), 'Keuangan') !== false):
                $result = 'ADM3';
                break;
            case ($data['id'] === 4 || strpos(strtolower($data['nama_role']), 'Marketing') !== false):
                $result = 'ADM4';
                break;
            case ($data['id'] === 5 || strpos(strtolower($data['nama_role']), 'Staff Marketing') !== false):
                $result = 'ADM5';
                break;
            case ($data['id'] === 6 || strpos(strtolower($data['nama_role']), 'Kantor Pusat') !== false):
                $result = 'ADM5';
                break;
            case ($data['id'] === 7 || strpos(strtolower($data['nama_role']), 'Kantor Cabang') !== false):
                $result = 'ADM6';
                break;
            case ($data['id'] === 8 || strpos(strtolower($data['nama_role']), 'Agen') !== false):
                $result = 'RSN1';
                break;
            case ($data['id'] === 9 || strpos(strtolower($data['nama_role']), 'Customer') !== false):
                $result = 'RSN2';
                break;
            default: // Pendaftar PWMP
                $result = 'PDF';
                break;
        }
        return $result;
    }

}
?>